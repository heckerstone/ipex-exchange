package ai.turbochain.ipex;

import java.util.Calendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import ai.turbochain.ipex.processor.CoinProcessorFactory;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MarketApplication.class)
public class KLineGeneratorJobTest {
	 @Autowired
	 private CoinProcessorFactory processorFactory;

  @Test
  public void contextLoads() throws Exception {
	  processorFactory.getProcessorMap().forEach((symbol,processor)->{
          Calendar calendar = Calendar.getInstance();
          //将秒、微秒字段置为0
          calendar.set(Calendar.HOUR_OF_DAY,0);
          calendar.set(Calendar.MINUTE,0);
          calendar.set(Calendar.SECOND,0);
          calendar.set(Calendar.MILLISECOND,0);
          long time = calendar.getTimeInMillis();
          int week = calendar.get(Calendar.DAY_OF_WEEK);
          int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
           
          if(dayOfMonth == 1){
              processor.generateKLine(1, Calendar.DAY_OF_MONTH, time);
          }
      });
  }

}

package ai.turbochain.ipex.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ai.turbochain.ipex.dao.base.BaseDao;
import ai.turbochain.ipex.entity.Department;

/**
 * @author jack
 * @date 2019年12月18日
 */
public interface DepartmentDao extends BaseDao<Department> {
}
